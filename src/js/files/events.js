import { deleteMoney, checkRemoveAddClass, noMoney, getRandom, addMoney, getRandom_2 } from './functions.js';
import { checkBoughtAirplanes, startGame, resetGame, drawCurrentAirplaneMainScreen, stopAnimation, config_game, prices } from './script.js';

const preloader = document.querySelector('.preloader');

// Объявляем слушатель событий "клик"

document.addEventListener('click', (e) => {
	let targetElement = e.target;
	let bank = +sessionStorage.getItem('money');
	let current_bet = +sessionStorage.getItem('current-bet');

	if (targetElement.closest('.main__button_privacy ') && preloader.classList.contains('_hide')) {
		preloader.classList.remove('_hide');
	}

	if (targetElement.closest('.preloader__button')) {
		preloader.classList.add('_hide');
	}

	if (targetElement.closest('.main__button_shop')) {
		document.querySelector('.main__body').classList.add('_shop');
	}
	if (targetElement.closest('.footer-shop__button-back') && document.querySelector('.main')) {
		document.querySelector('.main__body').classList.remove('_shop');
	}

	if (targetElement.closest('[data-shop-button="1"]')) {
		checkRemoveAddClass('.shop__item', '_selected', document.querySelectorAll('.shop__item')[0]);
		sessionStorage.setItem('current-airplane', 1);
		drawCurrentAirplaneMainScreen();
	}

	if (targetElement.closest('[data-shop-button="2"]') && !targetElement.closest('[data-shop-button="2"]').classList.contains('_bought')) {
		if (bank >= prices.price_2) {
			deleteMoney(prices.price_2, '.check');
			sessionStorage.setItem('airplane-2', true);
			checkBoughtAirplanes();
		} else noMoney('.check');
	} else if (targetElement.closest('[data-shop-button="2"]') && targetElement.closest('[data-shop-button="2"]').classList.contains('_bought')) {
		checkRemoveAddClass('.shop__item', '_selected', document.querySelectorAll('.shop__item')[1]);
		sessionStorage.setItem('current-airplane', 2);
		drawCurrentAirplaneMainScreen();
	}

	if (targetElement.closest('[data-shop-button="3"]') && !targetElement.closest('[data-shop-button="3"]').classList.contains('_bought')) {
		if (bank >= prices.price_3) {
			deleteMoney(prices.price_3, '.check');
			sessionStorage.setItem('airplane-3', true);
			checkBoughtAirplanes();
		} else noMoney('.check');
	} else if (targetElement.closest('[data-shop-button="3"]') && targetElement.closest('[data-shop-button="3"]').classList.contains('_bought')) {
		checkRemoveAddClass('.shop__item', '_selected', document.querySelectorAll('.shop__item')[2]);
		sessionStorage.setItem('current-airplane', 3);
		drawCurrentAirplaneMainScreen();
	}

	//game
	if (targetElement.closest('.block-bet__minus')) {
		if (current_bet > 50) {
			sessionStorage.setItem('current-bet', current_bet - 50);
			document.querySelector('.block-bet__coins').textContent = sessionStorage.getItem('current-bet');
		}
	}
	if (targetElement.closest('.block-bet__plus')) {
		if (bank - 50 > current_bet) {
			sessionStorage.setItem('current-bet', current_bet + 50);
			document.querySelector('.block-bet__coins').textContent = sessionStorage.getItem('current-bet');
		} else {
			noMoney('.check');
		}
	}


	if (targetElement.closest('.footer__button-bet') && config_game.state === 2) {
		stopAnimation();
		config_game.state = 3;
		document.querySelector('.footer__button-bet').classList.add('_hold');
		setTimeout(() => {
			resetGame();
		}, 2000);
		addMoney(config_game.current_win, '.check', 1000, 2000);
	}

	if (targetElement.closest('.footer__button-bet') && config_game.state === 1) {
		document.querySelector('.block-bet').classList.add('_hold');
		startGame();
	}

})